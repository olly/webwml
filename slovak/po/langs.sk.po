# Translation of langs.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2013.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: 2008-10-22 19:30+0200\n"
"PO-Revision-Date: 2013-05-14 13:12+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/language_names.wml:8
msgid "Arabic"
msgstr "arabčina"

#: ../../english/template/debian/language_names.wml:9
msgid "Armenian"
msgstr "arménčina"

#: ../../english/template/debian/language_names.wml:10
msgid "Finnish"
msgstr "fínčina"

#: ../../english/template/debian/language_names.wml:11
msgid "Croatian"
msgstr "chorvátčina"

#: ../../english/template/debian/language_names.wml:12
msgid "Danish"
msgstr "dánčina"

#: ../../english/template/debian/language_names.wml:13
msgid "Dutch"
msgstr "holandčina"

#: ../../english/template/debian/language_names.wml:14
msgid "English"
msgstr "angličtina"

#: ../../english/template/debian/language_names.wml:15
msgid "French"
msgstr "francúzština"

#: ../../english/template/debian/language_names.wml:16
msgid "Galician"
msgstr "galícijčina"

#: ../../english/template/debian/language_names.wml:17
msgid "German"
msgstr "nemčina"

#: ../../english/template/debian/language_names.wml:18
msgid "Italian"
msgstr "taliančina"

#: ../../english/template/debian/language_names.wml:19
msgid "Japanese"
msgstr "japončina"

#: ../../english/template/debian/language_names.wml:20
msgid "Korean"
msgstr "kórejčina"

#: ../../english/template/debian/language_names.wml:21
msgid "Spanish"
msgstr "španielčina"

#: ../../english/template/debian/language_names.wml:22
msgid "Portuguese"
msgstr "portugalčina"

#: ../../english/template/debian/language_names.wml:23
msgid "Portuguese (Brazilian)"
msgstr "portugalčina (Brazília)"

#: ../../english/template/debian/language_names.wml:24
msgid "Chinese"
msgstr "čínština"

#: ../../english/template/debian/language_names.wml:25
msgid "Chinese (China)"
msgstr "čínština (Čína)"

#: ../../english/template/debian/language_names.wml:26
msgid "Chinese (Hong Kong)"
msgstr "čínština (Hongkong)"

#: ../../english/template/debian/language_names.wml:27
msgid "Chinese (Taiwan)"
msgstr "čínština (Taiwan)"

#: ../../english/template/debian/language_names.wml:28
msgid "Chinese (Traditional)"
msgstr "čínština (tradičná)"

#: ../../english/template/debian/language_names.wml:29
msgid "Chinese (Simplified)"
msgstr "čínština (zjednodušená)"

#: ../../english/template/debian/language_names.wml:30
msgid "Swedish"
msgstr "švédčina"

#: ../../english/template/debian/language_names.wml:31
msgid "Polish"
msgstr "poľština"

#: ../../english/template/debian/language_names.wml:32
msgid "Norwegian"
msgstr "nórčina"

#: ../../english/template/debian/language_names.wml:33
msgid "Turkish"
msgstr "turečtina"

#: ../../english/template/debian/language_names.wml:34
msgid "Russian"
msgstr "ruština"

#: ../../english/template/debian/language_names.wml:35
msgid "Czech"
msgstr "čeština"

#: ../../english/template/debian/language_names.wml:36
msgid "Esperanto"
msgstr "esperanto"

#: ../../english/template/debian/language_names.wml:37
msgid "Hungarian"
msgstr "maďarčina"

#: ../../english/template/debian/language_names.wml:38
msgid "Romanian"
msgstr "rumunčina"

#: ../../english/template/debian/language_names.wml:39
msgid "Slovak"
msgstr "slovenčina"

#: ../../english/template/debian/language_names.wml:40
msgid "Greek"
msgstr "gréčtina"

#: ../../english/template/debian/language_names.wml:41
msgid "Catalan"
msgstr "katalánčina"

#: ../../english/template/debian/language_names.wml:42
msgid "Indonesian"
msgstr "indonézština"

#: ../../english/template/debian/language_names.wml:43
msgid "Lithuanian"
msgstr "litovčina"

#: ../../english/template/debian/language_names.wml:44
msgid "Slovene"
msgstr "slovinčina"

#: ../../english/template/debian/language_names.wml:45
msgid "Bulgarian"
msgstr "bulharčina"

#: ../../english/template/debian/language_names.wml:46
msgid "Tamil"
msgstr "tamilčina"

#. for now, the following are only needed if you intend to translate intl/l10n
#: ../../english/template/debian/language_names.wml:48
msgid "Afrikaans"
msgstr "afrikánčina"

#: ../../english/template/debian/language_names.wml:49
msgid "Albanian"
msgstr "albánčina"

#: ../../english/template/debian/language_names.wml:50
msgid "Asturian"
msgstr "astúrčina"

#: ../../english/template/debian/language_names.wml:51
msgid "Amharic"
msgstr "amharčina"

#: ../../english/template/debian/language_names.wml:52
msgid "Azerbaijani"
msgstr "azerbajdžančina"

#: ../../english/template/debian/language_names.wml:53
msgid "Basque"
msgstr "baskičtina"

#: ../../english/template/debian/language_names.wml:54
msgid "Belarusian"
msgstr "bieloruština"

#: ../../english/template/debian/language_names.wml:55
msgid "Bengali"
msgstr "bengálčina"

#: ../../english/template/debian/language_names.wml:56
msgid "Bosnian"
msgstr "bosniačtina"

#: ../../english/template/debian/language_names.wml:57
msgid "Breton"
msgstr "bretónčina"

#: ../../english/template/debian/language_names.wml:58
msgid "Cornish"
msgstr "kornčina"

#: ../../english/template/debian/language_names.wml:59
msgid "Estonian"
msgstr "estónčina"

#: ../../english/template/debian/language_names.wml:60
msgid "Faeroese"
msgstr "faerčina"

#: ../../english/template/debian/language_names.wml:61
msgid "Gaelic (Scots)"
msgstr "škótska gaelčina"

#: ../../english/template/debian/language_names.wml:62
msgid "Georgian"
msgstr "gruzínčina"

#: ../../english/template/debian/language_names.wml:63
msgid "Hebrew"
msgstr "hebrejčina"

#: ../../english/template/debian/language_names.wml:64
msgid "Hindi"
msgstr "hindčina"

#: ../../english/template/debian/language_names.wml:65
msgid "Icelandic"
msgstr "islandčina"

#: ../../english/template/debian/language_names.wml:66
msgid "Interlingua"
msgstr "interlingua"

#: ../../english/template/debian/language_names.wml:67
msgid "Irish"
msgstr "írčina"

#: ../../english/template/debian/language_names.wml:68
msgid "Kalaallisut"
msgstr "grónčina"

#: ../../english/template/debian/language_names.wml:69
msgid "Kannada"
msgstr "kannadčina"

#: ../../english/template/debian/language_names.wml:70
msgid "Kurdish"
msgstr "kurdčina"

#: ../../english/template/debian/language_names.wml:71
msgid "Latvian"
msgstr "lotyština"

#: ../../english/template/debian/language_names.wml:72
msgid "Macedonian"
msgstr "macedónčina"

#: ../../english/template/debian/language_names.wml:73
msgid "Malay"
msgstr "malajčina"

#: ../../english/template/debian/language_names.wml:74
msgid "Malayalam"
msgstr "malajálamčina"

#: ../../english/template/debian/language_names.wml:75
msgid "Maltese"
msgstr "maltčina"

#: ../../english/template/debian/language_names.wml:76
msgid "Manx"
msgstr "mančina"

#: ../../english/template/debian/language_names.wml:77
msgid "Maori"
msgstr "maorčina"

#: ../../english/template/debian/language_names.wml:78
msgid "Mongolian"
msgstr "mongolčina"

#: ../../english/template/debian/language_names.wml:80
msgid "Norwegian Bokm&aring;l"
msgstr "nórsky bokmål"

#: ../../english/template/debian/language_names.wml:82
msgid "Norwegian Nynorsk"
msgstr "nórsky nynorsk"

#: ../../english/template/debian/language_names.wml:83
msgid "Occitan (post 1500)"
msgstr "okcitánčina (po roku 1500)"

#: ../../english/template/debian/language_names.wml:84
msgid "Persian"
msgstr "perzština"

#: ../../english/template/debian/language_names.wml:85
msgid "Serbian"
msgstr "srbčina"

#: ../../english/template/debian/language_names.wml:86
msgid "Slovenian"
msgstr "slovinčina"

#: ../../english/template/debian/language_names.wml:87
msgid "Tajik"
msgstr "tadžičtina"

#: ../../english/template/debian/language_names.wml:88
msgid "Thai"
msgstr "thajčina"

#: ../../english/template/debian/language_names.wml:89
msgid "Tonga"
msgstr "tongčina"

#: ../../english/template/debian/language_names.wml:90
msgid "Twi"
msgstr "twi"

#: ../../english/template/debian/language_names.wml:91
msgid "Ukrainian"
msgstr "ukrajinčina"

#: ../../english/template/debian/language_names.wml:92
msgid "Vietnamese"
msgstr "vietnamčina"

#: ../../english/template/debian/language_names.wml:93
msgid "Welsh"
msgstr "waleština"

#: ../../english/template/debian/language_names.wml:94
msgid "Xhosa"
msgstr "xhosa"

#: ../../english/template/debian/language_names.wml:95
msgid "Yiddish"
msgstr "jidiš"

#: ../../english/template/debian/language_names.wml:96
msgid "Zulu"
msgstr "zuluština"
