# translation of webwml/english/po/organization.pot to Dutch
# Templates files for webwml modules
# Copyright (C) 2003,2004 Software in the Public Interest, Inc.
#
# Frans Pop <aragorn@tiscali.nl>, 2006.
# Frans Pop <elendil@planet.nl>, 2006, 2007, 2008, 2009, 2010.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2014, 2017, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml/po/organization.nl.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2018-07-04 16:39+0200\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "delegatiebericht"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "aanstellingsbericht"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>gemachtigde"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>gemachtigde"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "huidige"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "lid"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "coördinator"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Stabiele-release coördinator"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "grootmeester"

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "voorzitter"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "secretaris"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:66
msgid "Officers"
msgstr "Gezagsdragers"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:90
msgid "Distribution"
msgstr "Distributie"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:232
msgid "Communication and Outreach"
msgstr "Communicatie en doelgroepenwerking"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:235
msgid "Data Protection team"
msgstr "Gegevensbeschermingsteam"

#: ../../english/intro/organization.data:58
#: ../../english/intro/organization.data:239
msgid "Publicity team"
msgstr "Publiciteitsteam"

#: ../../english/intro/organization.data:60
#: ../../english/intro/organization.data:303
msgid "Support and Infrastructure"
msgstr "Ondersteuning en Infrastructuur"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:62
msgid "Debian Pure Blends"
msgstr "Specifieke uitgaven"

#: ../../english/intro/organization.data:69
msgid "Leader"
msgstr "Leider"

#: ../../english/intro/organization.data:71
msgid "Technical Committee"
msgstr "Technisch comité"

#: ../../english/intro/organization.data:85
msgid "Secretary"
msgstr "Secretaris"

#: ../../english/intro/organization.data:93
msgid "Development Projects"
msgstr "Ontwikkelingsprojecten"

#: ../../english/intro/organization.data:94
msgid "FTP Archives"
msgstr "FTP-archieven"

#: ../../english/intro/organization.data:96
msgid "FTP Masters"
msgstr "FTP-meesters"

#: ../../english/intro/organization.data:102
msgid "FTP Assistants"
msgstr "FTP-assistenten"

#: ../../english/intro/organization.data:107
msgid "FTP Wizards"
msgstr "FTP-grootmeesters"

#: ../../english/intro/organization.data:111
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:113
msgid "Backports Team"
msgstr "Backports-team"

#: ../../english/intro/organization.data:117
msgid "Individual Packages"
msgstr "Individuele pakketten"

#: ../../english/intro/organization.data:118
msgid "Release Management"
msgstr "Releasemanagement"

#: ../../english/intro/organization.data:120
msgid "Release Team"
msgstr "Release-team"

#: ../../english/intro/organization.data:133
msgid "Quality Assurance"
msgstr "Kwaliteitscontrole"

#: ../../english/intro/organization.data:134
msgid "Installation System Team"
msgstr "Installatiesysteem-team"

#: ../../english/intro/organization.data:135
msgid "Release Notes"
msgstr "Notities bij de uitgave"

#: ../../english/intro/organization.data:137
msgid "CD Images"
msgstr "CD-images"

#: ../../english/intro/organization.data:139
msgid "Production"
msgstr "Productie"

#: ../../english/intro/organization.data:147
msgid "Testing"
msgstr "Testing"

#: ../../english/intro/organization.data:149
msgid "Autobuilding infrastructure"
msgstr "Infrastructuur voor geautomatiseerde bouw van pakketten"

#: ../../english/intro/organization.data:151
msgid "Wanna-build team"
msgstr "Wanna-build-team"

#: ../../english/intro/organization.data:159
msgid "Buildd administration"
msgstr "Buildd-beheer"

#: ../../english/intro/organization.data:178
msgid "Documentation"
msgstr "Documentatie"

#: ../../english/intro/organization.data:183
msgid "Work-Needing and Prospective Packages list"
msgstr "Lijst van pakketten waaraan gewerkt moet worden of die eraan komen"

#: ../../english/intro/organization.data:186
msgid "Debian Live Team"
msgstr "Debian team voor live-media"

#: ../../english/intro/organization.data:187
msgid "Ports"
msgstr "Ports"

#: ../../english/intro/organization.data:222
msgid "Special Configurations"
msgstr "Speciale configuraties"

#: ../../english/intro/organization.data:225
msgid "Laptops"
msgstr "Laptops"

#: ../../english/intro/organization.data:226
msgid "Firewalls"
msgstr "Firewalls"

#: ../../english/intro/organization.data:227
msgid "Embedded systems"
msgstr "Embedded systemen"

#: ../../english/intro/organization.data:242
msgid "Press Contact"
msgstr "Perscontact"

#: ../../english/intro/organization.data:244
msgid "Web Pages"
msgstr "Webpagina’s"

#: ../../english/intro/organization.data:254
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:259
msgid "Outreach"
msgstr "Doelgroepenwerking"

#: ../../english/intro/organization.data:263
msgid "Debian Women Project"
msgstr "Vrouwen in Debian"

#: ../../english/intro/organization.data:271
msgid "Anti-harassment"
msgstr "Pestpreventie"

#: ../../english/intro/organization.data:275
msgid "Events"
msgstr "Evenementen"

#: ../../english/intro/organization.data:281
msgid "DebConf Committee"
msgstr "Debian conferentiecomité"

#: ../../english/intro/organization.data:288
msgid "Partner Program"
msgstr "Partnerprogramma"

#: ../../english/intro/organization.data:293
msgid "Hardware Donations Coordination"
msgstr "Coördinatie hardwaredonaties"

#: ../../english/intro/organization.data:306
msgid "User support"
msgstr "Gebruikersondersteuning"

#: ../../english/intro/organization.data:373
msgid "Bug Tracking System"
msgstr "Bugvolgsysteem"

#: ../../english/intro/organization.data:378
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Beheer van mailinglijsten en mailinglijstarchieven"

#: ../../english/intro/organization.data:386
msgid "New Members Front Desk"
msgstr "Aanmeldbalie voor nieuwe leden"

#: ../../english/intro/organization.data:392
msgid "Debian Account Managers"
msgstr "Beheerders van ontwikkelaarsaccounts"

#: ../../english/intro/organization.data:396
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Om een privébericht naar alle beheerders van ontwikkelaarsaccounts te sturen "
"moet u de GPG-sleutel 57731224A9762EA155AB2A530CA8D15BB24D96F2 gebruiken."

#: ../../english/intro/organization.data:397
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Beheerders van encryptiesleutels (PGP en GPG)"

#: ../../english/intro/organization.data:400
msgid "Security Team"
msgstr "Beveiligingsteam"

#: ../../english/intro/organization.data:412
msgid "Consultants Page"
msgstr "Pagina met Debian dienstverleners"

#: ../../english/intro/organization.data:417
msgid "CD Vendors Page"
msgstr "Pagina voor CD-verkopers"

#: ../../english/intro/organization.data:420
msgid "Policy"
msgstr "Beleid"

#: ../../english/intro/organization.data:425
msgid "System Administration"
msgstr "Systeembeheer"

#: ../../english/intro/organization.data:426
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Dit is het adres dat u moet gebruiken als er problemen optreden met een van "
"Debian’s machines, inclusief wachtwoord problemen, of als er een pakket "
"geïnstalleerd moet worden."

#: ../../english/intro/organization.data:435
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Als u hardware problemen hebt met Debian machine, gelieve dan de <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machine</a> pagina te "
"raadplegen. Deze behoort per machine over de nodige beheerdersinformatie te "
"bevatten."

#: ../../english/intro/organization.data:436
msgid "LDAP Developer Directory Administrator"
msgstr "Beheerder van de LDAP-gids van ontwikkelaars"

#: ../../english/intro/organization.data:437
msgid "Mirrors"
msgstr "Spiegelservers"

#: ../../english/intro/organization.data:444
msgid "DNS Maintainer"
msgstr "DNS-beheerder"

#: ../../english/intro/organization.data:445
msgid "Package Tracking System"
msgstr "Pakketvolgsysteem"

#: ../../english/intro/organization.data:447
msgid "Treasurer"
msgstr "Penningmeester"

#: ../../english/intro/organization.data:453
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Aanvragen voor het gebruik van het <a name=\"trademark\" href=\"m4_HOME/"
"trademark\">handelsmerk</a>"

#: ../../english/intro/organization.data:456
msgid "Salsa administrators"
msgstr "Salsa-beheerders"

#: ../../english/intro/organization.data:460
msgid "Alioth administrators"
msgstr "Alioth-beheerders"

#: ../../english/intro/organization.data:473
msgid "Debian for children from 1 to 99"
msgstr "Debian voor kinderen van 1 tot 99"

#: ../../english/intro/organization.data:476
msgid "Debian for medical practice and research"
msgstr "Debian voor medische toepassingen en onderzoek"

#: ../../english/intro/organization.data:479
msgid "Debian for education"
msgstr "Debian voor het onderwijs"

#: ../../english/intro/organization.data:484
msgid "Debian in legal offices"
msgstr "Debian in de rechtspraak"

#: ../../english/intro/organization.data:488
msgid "Debian for people with disabilities"
msgstr "Debian voor mensen met een handicap"

#: ../../english/intro/organization.data:492
msgid "Debian for science and related research"
msgstr "Debian voor wetenschappelijke toepassingen en onderzoek"

#: ../../english/intro/organization.data:495
msgid "Debian for astronomy"
msgstr "Debian voor de sterrenkunde"

#~ msgid "Testing Security Team"
#~ msgstr "Beveiligingsteam voor Testing"

#~ msgid "Security Audit Project"
#~ msgstr "Beveiligingsauditproject"

#~ msgid "current Debian Project Leader"
#~ msgstr "huidige projectleider van Debian"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (Niet actief: is niet uitgebracht met squeeze)"

#~ msgid "Bits from Debian"
#~ msgstr "Stukjes van Debian"

#~ msgid "DebConf chairs"
#~ msgstr "DebConf-leiding"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Beheerders van de encryptiesleutels van Debian Maintainers (DM)"

#~ msgid "Publicity"
#~ msgstr "Publiciteit"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Live System Team"
#~ msgstr "Livesysteem-team"
