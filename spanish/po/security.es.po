# Translation of the Debian website: security
# Copyright (C) 2003-2017 SPI, Inc.
#
# Javier Fernandez-Sanguino <jfs@debian.org>, 2017
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:23+0200\n"
"Last-Translator: Javier Fernandez-Sanguino <jfs@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/security/dsa.rdf.in:16
msgid "Debian Security"
msgstr "Seguridad en Debian"

#: ../../english/security/dsa.rdf.in:19
msgid "Debian Security Advisories"
msgstr "Avisos de seguridad de Debian"

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr "P"

#: ../../english/security/index.include:17
msgid ""
"<a href=\\\"undated/\\\">undated</a> security advisories, included for "
"posterity"
msgstr ""
"avisos de seguridad <a href=\\\"undated/\\\">sin fechar</a>, incluidos para "
"la posteridad"

#: ../../english/security/make-ref-table.pl:81
msgid "Mitre CVE dictionary"
msgstr "Diccionario CVE de Mitre"

#: ../../english/security/make-ref-table.pl:84
msgid "Securityfocus Bugtraq database"
msgstr "Base de datos Bugtraq de Securityfocus"

#: ../../english/security/make-ref-table.pl:88
msgid "CERT Advisories"
msgstr "Avisos del CERT"

#: ../../english/security/make-ref-table.pl:92
msgid "US-CERT vulnerabilities Notes"
msgstr "Notas de vulnerabilidades US-CERT"

#: ../../english/template/debian/security.wml:11
msgid "Source:"
msgstr "Fuentes:"

#: ../../english/template/debian/security.wml:15
msgid "Architecture-independent component:"
msgstr "Componentes independientes de la arquitectura:"

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:22
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">original advisory</a>."
msgstr ""
"Las sumas MD5 de los ficheros que se listan están disponibles en el <a href="
"\"<get-var url />\">aviso original</a>."

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:30
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">revised advisory</a>."
msgstr ""
"Las sumas MD5 de los ficheros que se listan están disponibles en el <a href="
"\"<get-var url />\">aviso revisado</a>."

#: ../../english/template/debian/security.wml:42
msgid "Debian Security Advisory"
msgstr "Aviso de seguridad de Debian"

#: ../../english/template/debian/security.wml:47
msgid "Date Reported"
msgstr "Fecha del informe"

#: ../../english/template/debian/security.wml:50
msgid "Affected Packages"
msgstr "Paquetes afectados"

#: ../../english/template/debian/security.wml:72
msgid "Vulnerable"
msgstr "Vulnerable"

#: ../../english/template/debian/security.wml:75
msgid "Security database references"
msgstr "Referencias a bases de datos de seguridad"

#: ../../english/template/debian/security.wml:78
msgid "More information"
msgstr "Información adicional"

#: ../../english/template/debian/security.wml:84
msgid "Fixed in"
msgstr "Arreglado en"

#: ../../english/template/debian/securityreferences.wml:16
msgid "BugTraq ID"
msgstr "Id. en BugTraq"

#: ../../english/template/debian/securityreferences.wml:60
msgid "Bug"
msgstr "error"

#: ../../english/template/debian/securityreferences.wml:76
msgid "In the Debian bugtracking system:"
msgstr "En el sistema de seguimiento de errores de Debian:"

#: ../../english/template/debian/securityreferences.wml:79
msgid "In the Bugtraq database (at SecurityFocus):"
msgstr "En la base de datos de Bugtraq (en SecurityFocus):"

#: ../../english/template/debian/securityreferences.wml:82
msgid "In Mitre's CVE dictionary:"
msgstr "En el diccionario CVE de Mitre:"

#: ../../english/template/debian/securityreferences.wml:85
msgid "CERT's vulnerabilities, advisories and incident notes:"
msgstr "Notas y avisos de incidentes y vulnerabilidades en CERT:"

#: ../../english/template/debian/securityreferences.wml:88
msgid "No other external database security references currently available."
msgstr ""
"No se dispone, de momento, de referencias a otras bases de datos de "
"seguridad externas."

#~ msgid "CERT alerts"
#~ msgstr "Alertas del CERT"
